var express = require('express');
var app = express();
//cors
// var cors = require("cors");
// const corsOpts = {
//   origin: "0.0.0.0",
//   methods: ["GET", "POST","PUT"],
//   allowedHeaders: ["Content-Type"],
// };
//   app.use(cors(corsOpts));


var cors = require('cors');
var app = express();
app.use(cors());
app.use(function(req, res, next) {
   res.header("Access-Control-Allow-Origin", "*");
   res.header('Access-Control-Allow-Methods', 'DELETE, PUT, GET, POST');
   res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
   next();
});


// Declaring global values to handle file and DB module
global.__root   = __dirname + '/';
global.__db   = require(__dirname + '/db.js');

// Master Management Controller API base
var MasterManagementController = require(__root + 'master_specialists/MasterController.js');
app.use('/api/master', MasterManagementController);

// users Management Controller API base
var UserManagementController = require(__root + 'users/UserController.js');
app.use('/api/users', UserManagementController);

// Admin Controller API base
var AdminController = require(__root + 'admin/AdminController.js');
app.use('/api/admin', AdminController);


// Appointment Management Controller API base
var AppointmentManagementController = require(__root + 'appointment/AppointmentController.js');
app.use('/api/appointment', AppointmentManagementController);
// Doctor Management
// Doctor Management Controller API base
var DoctorManagementController = require(__root + 'doctor/DoctorController.js');
app.use('/api/doctor', DoctorManagementController);


module.exports = app;
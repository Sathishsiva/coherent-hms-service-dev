var nodemailer = require('nodemailer');

function mail(data, next) {
    var transporter = nodemailer.createTransport(
        {
            host : 'mail.coherent.in',
            port : '587',
            tls: {
                rejectUnauthorized: false
            },
            auth:
            {
                user: 'noreply@coherent.in',
                pass: 'Coherent@2021'
            }
        });


    var mailOptions =
    {
        from: 'noreply@coherent.in',
        to: data.email_id,
        subject: data.subject,
        text: data.random_number
    };

    transporter.sendMail(mailOptions, function (error, info) {
        if (error) {
            console.log("error", error);
        }
        else {
            console.log('Email sent: ' + info.response);
            next();
        }
    });
}

module.exports = mail;
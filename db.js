global.Sequelize = require('sequelize');


    //Connnecting to database
const sequelize = new Sequelize({
        username : 'remote',
        password : 'coherent@123',
        database : 'coherent_hms_dev',
        host: '95.217.209.135',
        dialect: 'mysql',
        logging: false
    })
    //DB connectivity check
try {
    sequelize.authenticate();
    console.log('Connection has been established successfully.');
} catch (error) {
    console.error('Unable to connect to the database:', error);
}
var master_specialists = sequelize.define("master_specialists", 
{
    speciallation_name: { type: Sequelize.STRING, allowNull: false },
    speciallation_code: { type: Sequelize.STRING, allowNull: false },
    speciallation_type: { type: Sequelize.STRING, allowNull: false },
    is_active:          { type: Sequelize.TINYINT, default : true,allowNull: true },
    deleted_flag:       { type: Sequelize.TINYINT, default : true,allowNull: true },
    createdby:          { type: Sequelize.SMALLINT, allowNull: true },
    updatedby:          { type: Sequelize.SMALLINT, allowNull: true}
});

var users = sequelize.define("users", 
{
    user_name:          { type: Sequelize.STRING, allowNull: false },
    password:           { type: Sequelize.STRING, allowNull: false },
    full_name:          { type: Sequelize.STRING, allowNull: false },
    email_id:           { type: Sequelize.STRING, allowNull: false },
    user_random_number: { type: Sequelize.STRING, allowNull: true },
    mobile_number:      { type: Sequelize.STRING, allowNull: false },
    gender:             { type: Sequelize.STRING, allowNull: false },
    education:          { type: Sequelize.STRING, allowNull: true },
    experience:         { type: Sequelize.STRING, allowNull: true },
    is_doctor:          { type: Sequelize.TINYINT, allowNull: true },
    is_active:          { type: Sequelize.TINYINT, default : true,allowNull: true },
    deleted_flag:       { type: Sequelize.TINYINT, default : true,allowNull: true },
    createdby:          { type: Sequelize.SMALLINT, allowNull: true },
    updatedby:          { type: Sequelize.SMALLINT, allowNull: true}
});


var doctor_specialist_mapping = sequelize.define("doctor_specialist_mapping", 
{
    user_id_fk:         { type: Sequelize.SMALLINT, allowNull: false },
    specialist_id_fk:   { type: Sequelize.SMALLINT, allowNull: false },
    is_active:          { type: Sequelize.TINYINT, default : true,allowNull: true },
    deleted_flag:       { type: Sequelize.TINYINT, default : true,allowNull: true },
    createdby:          { type: Sequelize.SMALLINT, allowNull: true },
    updatedby:          { type: Sequelize.SMALLINT, allowNull: true}
});

var patient_personal_details = sequelize.define("patient_personal_details",
{

    patient_id_number:  { type: Sequelize.STRING, allowNull: true },
    first_name:         { type: Sequelize.STRING, allowNull: false },
    last_name:          { type: Sequelize.STRING, allowNull: false },
    age:                { type: Sequelize.SMALLINT, allowNull: false },
    mobile_number:      { type: Sequelize.STRING, allowNull: false },
    email_id:           { type: Sequelize.STRING, allowNull: false },
    gender:             { type: Sequelize.STRING, allowNull: false },
    is_active:          { type: Sequelize.TINYINT, default : true,allowNull: true },
    deleted_flag:       { type: Sequelize.TINYINT, default : true,allowNull: true },
    createdby:          { type: Sequelize.SMALLINT, allowNull: true },
    updatedby:          { type: Sequelize.SMALLINT, allowNull: true}
    

});

var patient_medical_history = sequelize.define("patient_medical_details",
{
    patient_id_fk:      { type: Sequelize.SMALLINT, allowNull: false },
    height:             { type: Sequelize.SMALLINT, allowNull: false },
    weight:             { type: Sequelize.SMALLINT, allowNull: false },
    sugar_level:        { type: Sequelize.STRING, allowNull: true },
    sugar_choose:       { type: Sequelize.STRING, allowNull: true },
    bp_level:           { type: Sequelize.STRING, allowNull: true },
    bp_choose:          { type: Sequelize.STRING, allowNull: true },
    heamoglobin_level:  { type: Sequelize.SMALLINT, allowNull: false },
    heamoglobin_choose: { type: Sequelize.STRING, allowNull: true },
    surgeries_in_past:  { type: Sequelize.STRING },
    surgeries_choose:   { type: Sequelize.STRING},
    allergies:          { type: Sequelize.STRING, default : true,allowNull: true },
    allergies_choose:   { type: Sequelize.STRING, default : true,allowNull: true },
    is_active:          { type: Sequelize.TINYINT, default : true,allowNull: true },
    deleted_flag:       { type: Sequelize.TINYINT, default : true,allowNull: true },
    createdby:          { type: Sequelize.SMALLINT, allowNull: true },
    updatedby:          { type: Sequelize.SMALLINT, allowNull: true}
});

var patient_address_details = sequelize.define("patient_address_details",
{
    patient_id_fk:      { type: Sequelize.SMALLINT, allowNull: false },
    door_no:            { type: Sequelize.SMALLINT, allowNull: false },
    street_name:        { type: Sequelize.STRING, allowNull: false },
    city_name:          { type: Sequelize.STRING, allowNull: false },
    state_name:         { type: Sequelize.STRING, allowNull: false },
    landmark:           { type: Sequelize.STRING, allowNull: false },
    description:        { type: Sequelize.STRING, allowNull: false },
    is_active:          { type: Sequelize.TINYINT, default : true,allowNull: true },
    deleted_flag:       { type: Sequelize.TINYINT, default : true,allowNull: true },
    createdby:          { type: Sequelize.SMALLINT, allowNull: true },
    updatedby:          { type: Sequelize.SMALLINT, allowNull: true}
});

var patient_doctor_mapping = sequelize.define("patient_doctor_mapping", 
{
    patient_id_fk:      { type: Sequelize.SMALLINT, allowNull: false },
    user_id_fk:         { type: Sequelize.SMALLINT, allowNull: false },
    is_active:          { type: Sequelize.TINYINT, default : true,allowNull: true },
    deleted_flag:       { type: Sequelize.TINYINT, default : true,allowNull: true },
    createdby:          { type: Sequelize.SMALLINT, allowNull: true },
    updatedby:          { type: Sequelize.SMALLINT, allowNull: true}
});

var roles = sequelize.define("roles",
{
    role_name:          { type: Sequelize.STRING, allowNull: false },
    is_active:          { type: Sequelize.TINYINT, default : true,allowNull: true },
    deleted_flag:       { type: Sequelize.TINYINT, default : true,allowNull: true },
});

var user_role_mapping = sequelize.define("user_role_mapping", 
{
    role_id_fk:         { type: Sequelize.SMALLINT, allowNull: false },
    user_id_fk:         { type: Sequelize.SMALLINT, allowNull: false },
    is_active:          { type: Sequelize.TINYINT, default : true,allowNull: true },
    deleted_flag:       { type: Sequelize.TINYINT, default : true,allowNull: true },
    createdby:          { type: Sequelize.SMALLINT, allowNull: true },
    updatedby:          { type: Sequelize.SMALLINT, allowNull: true}
});

var Appointment_doctor = sequelize.define("Appointment_doctor",
    {
       
        name:               { type: Sequelize.STRING, allowNull: false },
        patient_name:       { type: Sequelize.STRING, allowNull: false },
        age:                { type: Sequelize.SMALLINT, allowNull: false },
        appointment_date:   { type: Sequelize.STRING, allowNull: false},
        mobile_number:      { type: Sequelize.STRING, allowNull: false },
        gender:             { type: Sequelize.STRING, allowNull: false },
        description:        { type: Sequelize.STRING, allowNull: false },
        user_id_fk:         { type: Sequelize.STRING, allowNull: false },
        is_active:          { type: Sequelize.TINYINT, default : true,allowNull: true },
        deleted_flag:       { type: Sequelize.TINYINT, default : true,allowNull: true },
        createdby:          { type: Sequelize.SMALLINT, allowNull: true },
        updatedby:          { type: Sequelize.SMALLINT, allowNull: true}
        
    });

    var prescription = sequelize.define("prescription",
    {
        patient_id_fk:   { type: Sequelize.SMALLINT, allowNull: false },
        user_id_fk:   { type: Sequelize.SMALLINT, allowNull: false },
        medicine_name:   { type: Sequelize.STRING, allowNull: false }, 
        medicine_mor:       { type: Sequelize.TINYINT, default : true,allowNull: true },
        medicine_aft:       { type: Sequelize.TINYINT, default : true,allowNull: true },
        medicine_eve:       { type: Sequelize.TINYINT, default : true,allowNull: true },
        food_taken:       { type: Sequelize.STRING, default : true,allowNull: true },
        days_of_medicine:       { type: Sequelize.STRING, default : true,allowNull: true },
        quantity_of_medicine:       { type: Sequelize.STRING, default : true,allowNull: true },
        is_active:       { type: Sequelize.TINYINT, default : true,allowNull: true },
        deleted_flag:    { type: Sequelize.TINYINT, default : true,allowNull: true },
        createdby:       { type: Sequelize.SMALLINT, allowNull: true },
        updatedby:       { type: Sequelize.SMALLINT, allowNull: true}
    });

    var user_personal_details = sequelize.define("user_personal_details",
    {
           user_id_fk:      { type: Sequelize.SMALLINT, allowNull: false },
            dob:            { type: Sequelize.STRING, allowNull: false },
        door_no:            { type: Sequelize.SMALLINT, allowNull: false },
        street_name:        { type: Sequelize.STRING, allowNull: false },
        city_name:          { type: Sequelize.STRING, allowNull: false },
        state_name:         { type: Sequelize.STRING, allowNull: false },
        pincode:            { type: Sequelize.STRING, allowNull: false },
        discription:            { type: Sequelize.STRING, allowNull: false },
        is_active:          { type: Sequelize.TINYINT, default : true,allowNull: true },
        deleted_flag:       { type: Sequelize.TINYINT, default : true,allowNull: true },
        createdby:          { type: Sequelize.SMALLINT, allowNull: true },
        updatedby:          { type: Sequelize.SMALLINT, allowNull: true}
    });
    
    var user_image_mapping = sequelize.define("user_image_mapping", 
{
    user_id_fk:      { type: Sequelize.SMALLINT, allowNull: false },
    image:              { type: Sequelize.STRING, allowNull: true },
    is_active:          { type: Sequelize.TINYINT, default : true,allowNull: true },
    deleted_flag:       { type: Sequelize.TINYINT, default : true,allowNull: true },
    createdby:          { type: Sequelize.SMALLINT, allowNull: true },
    updatedby:          { type: Sequelize.SMALLINT, allowNull: true}
});

var patient_report_details = sequelize.define("patient_report_details",
    {
        patient_id_fk:         { type: Sequelize.SMALLINT, allowNull: false },
        report:              { type: Sequelize.STRING, allowNull: true },
        is_active:          { type: Sequelize.TINYINT, default : true,allowNull: true },
        deleted_flag:       { type: Sequelize.TINYINT, default : true,allowNull: true },
        createdby:          { type: Sequelize.SMALLINT, allowNull: true },
        updatedby:          { type: Sequelize.SMALLINT, allowNull: true}
    });

    var User_validation = sequelize.define("User_validation", 
{
    email_id:           { type: Sequelize.STRING, allowNull: false },
    otp:                { type: Sequelize.SMALLINT, allowNull: false },
    is_active:          { type: Sequelize.TINYINT, default : true,allowNull: true },
    deleted_flag:       { type: Sequelize.TINYINT, default : true,allowNull: true },
    createdby:          { type: Sequelize.SMALLINT, allowNull: true },
    updatedby:          { type: Sequelize.SMALLINT, allowNull: true}
});

sequelize.sync();

//Export initialised models
module.exports = {

Users : users,
Roles : roles,
Master_specialists : master_specialists,
Doctor_specialist_mapping : doctor_specialist_mapping,
Patient_personal_details : patient_personal_details,
Patient_medical_history : patient_medical_history,
Patient_address_details : patient_address_details,
Patient_doctor_mapping : patient_doctor_mapping,
User_role_mapping : user_role_mapping,
Appointment_doctor:Appointment_doctor,
prescription:prescription,
user_personal_details:user_personal_details,
user_image_mapping:user_image_mapping,
patient_report_details:patient_report_details,
User_validation:User_validation
};

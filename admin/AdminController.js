var express       = require('express'),
    router        = express.Router(),
    editJsonFile  = require('edit-json-file')
    bodyParser    = require('body-parser');
const jwt = require('jsonwebtoken');
const jwt_decode = require('jwt-decode');
var verifyToken    = require('../verifyToken.js');
var config = editJsonFile(__root +"config.json");
const moment= require('moment');
const date = require('date-and-time');
const multer = require('multer');
const  Sequelize = require('sequelize');
const sequelize = require('sequelize');
const Op = Sequelize.Op

const url = 'http://65.108.240.252/profile/images/'
//const { where } = require('sequelize/types');

// Storage Engin That Tells/Configures Multer for where (destination) and how (filename) to save/upload our files
const fileStorageEngine = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, "/data/www/profile/images"); //important this is a direct path fron our current file to storage location
  },
  filename: (req, file, cb) => {
    cb(null,Date.now()+'_' +file.originalname);
  },
})

const fileFilter = (req, file, cb) => {
  // reject a file
  if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png'|| file.mimetype === 'image/jpg') {
    cb(null, true);
  } else {
    cb(null, false);
  }
};
// The Multer Middleware that is passed to routes that will receive income requests with file data (multipart/formdata)
// You can create multiple middleware each with a different storage engine config so save different files in different locations on server
const upload = multer({ storage: fileStorageEngine, limits: {
  fileSize: 1024 * 1024 * 5
},
   fileFilter: fileFilter 
});


router.use(bodyParser.json());

//Declare the database tables
var Patient_medical_history = __db.Patient_medical_history;
var Patient_personal_details = __db.Patient_personal_details;
var Patient_address_details = __db.Patient_address_details;
var Patient_doctor_mapping = __db.Patient_doctor_mapping;
var Users = __db.Users;
var patient_report_details = __db.patient_report_details;
var Appointment_doctor=__db.Appointment_doctor;
console.log("test")

//Create new patient
router.post('/new',verifyToken,function (req, res) { 
    //Get token from the header
	var token   = req.headers.authorization;
	//Decode the token into json object
    var decoded = jwt_decode(token);
    var created = 
    {
    	 patient_id_number : "PAT"+Math.floor(10000 + Math.random() * 90000),
    	 createdby : decoded.id
    }
    var request = {...created,...req.body};
 
	Patient_personal_details.create(request).then(function(patient_data){
		var patient_medical_payload = 
		{
			patient_id_fk     : patient_data.id,
			height            : request.height,
			weight            : request.weight,
			sugar_level       : request.sugar_level,
			bp_level          : request.bp_level,
			heamoglobin_level : request.heamoglobin_level,
            surgeries_in_past : request.surgeries_in_past,
          appointment_date  : request.appointment_date,
            allergies         : request.allergies,
			createdby         : patient_data.createdby

		}
		Patient_medical_history.create(patient_medical_payload).then(function(patient_medical)
		{
        	var patient_address_payload = 
        	{
				patient_id_fk : patient_data.id,
				door_no       : request.door_no,
				street_name   : request.street_name,
				city_name     : request.city_name,
				state_name    : request.state_name,
				landmark      : request.landmark,
				description   : request.description,
				createdby     : patient_data.createdby
				
			}
			Patient_address_details.create(patient_address_payload).then(function(patient_address)
			{
				var mapping_payload = 
				{
					"patient_id_fk" : patient_data.id,
			        "user_id_fk"    : request.user_id,
			        "createdby"     : patient_data.createdby
			        
				}
				Patient_doctor_mapping.create(mapping_payload).then(function(patient_mapping)
				{
					res.send("Patient created successfully");
				})
			})
		})
	})
});


//Get Patient List
router.get('/patient/list',verifyToken,function(req,res)
{
    console.log("id");
    var response_array = [];
    Patient_personal_details.findAll({where:{is_active:1},order:[['id','DESC']]}).then(function(patient_basic)
    {
     	var patient_payload = patient_basic;
        var processItems = function(x)
        {
            if( x < patient_payload.length ) 
            {
            	Patient_medical_history.findOne({where:{patient_id_fk:patient_payload[x].id}}).then(function(patient_data)
            	{
                	var data = {...patient_payload[x].dataValues,...patient_data.dataValues}
            		Patient_address_details.findOne({where:{patient_id_fk:patient_payload[x].id}}).then(function(patient_medical)
            		{
                        var additional_data ={...data,...patient_medical.dataValues}
                        Patient_doctor_mapping.findOne({where:{patient_id_fk:patient_payload[x].id}}).then(function(map_data){
                        	Users.findOne({where:{id:map_data.user_id_fk}}).then(function(user_data)
                        	{
                        		var user = 
                        		{
                        			"user_name" : user_data.dataValues.user_name
                        		}
                        		var final_data = {...additional_data,...user}
	                			response_array.push(final_data)
	                			processItems(x+1);
                	    	})
                        })
                        
            		})
        		})
            }
            else 
            {
          		res.status(200).send({"patient_data":response_array})
        	}
    	}
    	processItems(0)
  	})
});



//Get Patient List
router.get('/patient/list/today',verifyToken,function(req,res)
{
	var today = new Date().toLocaleDateString();
    var response_array = [];
    Patient_personal_details.findAll({where:{is_active:1},order:[['id','DESC']]}).then(function(patient_basic)
    {

     	var patient_payload = patient_basic;
        var processItems = function(x)
        {
            if( x < patient_payload.length) 
            {
            	Patient_medical_history.findOne({where:{patient_id_fk:patient_payload[x].id}}).then(function(patient_data)
            	{
                	var data = {...patient_payload[x].dataValues,...patient_data.dataValues}
            		Patient_address_details.findOne({where:{patient_id_fk:patient_payload[x].id}}).then(function(patient_medical)
            		{
                        var additional_data ={...data,...patient_medical.dataValues}
                        Patient_doctor_mapping.findOne({where:{patient_id_fk:patient_payload[x].id}}).then(function(map_data){
                        	Users.findOne({where:{id:map_data.user_id_fk}}).then(function(user_data)
                        	{
                        		var user = 
                        		{
                        			"user_name" : user_data.dataValues.user_name
                        		}
                        		var final_data = {...additional_data,...user}
	                			var date = moment(final_data.createdAt).format("MM/DD/YYYY");
	                			var current_date = Date.parse(date);
                                var today_date = Date.parse(today);
                                console.log(current_date)
                                console.log(today_date)
                        	    if(today_date == current_date)
                        	    {
	                			  response_array.push(final_data)
	                		    }
	                			processItems(x+1);
                	    	})
                        })
                        
            		})
        		})
            }
            else 
            {
          		res.status(200).send({"patient_data":response_array})
        	}
    	}
    	processItems(0)
  	})
});


//Get Patient List
router.get('/patient/list/between/dates',verifyToken,function(req,res)
{

    var response_array = [];
    Patient_personal_details.findAll({where:{is_active:1}}).then(function(patient_basic)
    {
     	var patient_payload = patient_basic;
        var processItems = function(x)
        {
            if( x < patient_payload.length ) 
            {
            	Patient_medical_history.findOne({where:{patient_id_fk:patient_payload[x].id}}).then(function(patient_data)
            	{
                	var data = {...patient_payload[x].dataValues,...patient_data.dataValues}
            		Patient_address_details.findOne({where:{patient_id_fk:patient_payload[x].id}}).then(function(patient_medical)
            		{
                        var additional_data ={...data,...patient_medical.dataValues}
                        Patient_doctor_mapping.findOne({where:{patient_id_fk:patient_payload[x].id}}).then(function(map_data){
                        	Users.findOne({where:{id:map_data.user_id_fk}}).then(function(user_data)
                        	{
                        		var user = 
                        		{
                        			"user_name" : user_data.dataValues.user_name
                        		}
                        		var final_data = {...additional_data,...user}
	                			var current_date = final_data.createdAt.toLocaleDateString();
	                			var date = moment(current_date).format("MM/DD/YYYY");
								var from_date = Date.parse(req.body.start_date);
                                var to_date = Date.parse(req.body.end_date);
                                var check_date = Date.parse(date);
                               if(check_date <= to_date && check_date >= from_date)  
                           	    {
	                			response_array.push(final_data)
	                		    }
	                			processItems(x+1);
                	    	})
                        })
                        
            		})
        		})
            }
            else 
            {
          		res.status(200).send({"patient_data":response_array})
        	}
    	}
    	processItems(0)
  	})
});



//To deactivate the Patient data
router.put('/deactivate/:id',verifyToken,function (req, res) 
{  
	//Get token from the header
	var token   = req.headers.authorization;
	//Decode the token into json object
    var decoded = jwt_decode(token);
    
    Patient_personal_details.update({is_active:'0',updatedby:decoded.id},{where:{id:req.params.id}}).then(function(user_update)
    {
        Patient_medical_history.update({is_active:'0',updatedby:decoded.id},{where:{patient_id_fk:req.params.id}}).then(function(patient_data){})
        Patient_address_details.update({is_active:'0',updatedby:decoded.id},{where:{patient_id_fk:req.params.id}}).then(function(data){})
        Patient_doctor_mapping.update({is_active:'0',updatedby:decoded.id},{where:{patient_id_fk:req.params.id}}).then(function(data){})
        if(user_update > 0) {res.status(200).send("User deactivated successfully")}
    })
    
 });


//Get Patient data by mobile number
router.get('/list/patient/data/:mobile_number',verifyToken,function(req,res)
{
   Patient_personal_details.findOne({where: {mobile_number: req.params.mobile_number}}).then(function( details)
   {
        Patient_medical_history.findOne({where:{patient_id_fk:details.id}}).then(function(Patient_data)
        {
            Patient_address_details.findOne({where:{patient_id_fk:details.id}}).then(function(Patient_details)
            {
                Patient_doctor_mapping.findOne({where:{patient_id_fk:details.id}}).then(function(Patient_mappings)
                {
                    Users.findOne({where:{id:details.id},attributes:['user_name']}).then(function(doctor_mappings)
                    {
                        var data ={...details.dataValues,...Patient_data.dataValues,...Patient_details.dataValues,...Patient_mappings.dataValues,...doctor_mappings.dataValues}
                        return res.send(data);
                    })
                })
            })
        })
    })
});


//Update Patient data
router.put('/update/patient/data/:id',verifyToken,function(req,res)
{
    var id = req.params.id;
    //Get token from the header
    var token   = req.headers.authorization;
    //Decode the token into json object
    var decoded = jwt_decode(token);
    var updated = { "updatedby" : decoded.id }
    var request = {...req.body,...updated}
    Patient_personal_details.update(request,{where:{id:id,is_active:'1'}}).then(function(user_update)
    {
        Patient_medical_history.update(request,{where:{id:id}}).then(function(medical_update){})
        Patient_address_details.update(request,{where:{id:id}}).then(function(address_update){})
        Patient_doctor_mapping.update(request,{where:{patient_id_fk:id}}).then(function(doctor){})
        if (user_update == 1) 
        {
            res.send("patient updated successfully.");
        } 
        else
        {
            res.send("Error in updating Users");
        }
    })
})




// doctor count
router.get('/count/doctor/list',function(req,res){	
	Patient_personal_details. findAndCountAll({where:{ is_active:'1' }}).then(function(users){ 
		var date_time = new Date();
         console.log(date_time);
		return res.send({count:users.count})
	})
})

// patient count
//patient get count list
router.get('/get/count/patient',verifyToken,function(req,res)
{
    var response_array = [];
    var today = new Date().toLocaleDateString();
    var count = 0;
    Patient_personal_details.findAll({where:{is_active:'1'}}).then(function(patient_data)
    {
        var processItems = function(x)
        {
            if( x < patient_data.length )
            {
                var date = moment(patient_data[x].createdAt).format("MM/DD/YYYY");
                var current_date = Date.parse(date);
                var today_date = Date.parse(today);
                if(current_date == today_date)
                {
                    count++;
                }
                processItems(x+1);
            }
        }
        processItems(0)
        res.status(200).send({"count":count})
    })
});


//Get Patient data by ID
router.get('/list/patient/data/find/:id',verifyToken,function(req,res)
{
   Patient_personal_details.findOne({where: {id: req.params.id}}).then(function( details)
   {
        Patient_medical_history.findOne({where:{patient_id_fk:details.id}}).then(function(Patient_data)
        {
            Patient_address_details.findOne({where:{patient_id_fk:details.id}}).then(function(Patient_details)
            {
                Patient_doctor_mapping.findOne({where:{patient_id_fk:details.id}}).then(function(Patient_mappings)
                {
                    Users.findOne({where:{id:details.id},attributes:['user_name']}).then(function(doctor_mappings)
                    {
                        var data ={...details.dataValues,...Patient_data.dataValues,...Patient_details.dataValues,...Patient_mappings.dataValues,...doctor_mappings.dataValues}
                        return res.send(data);
                    })
                })
            })
        })
    })
});


// combine count for Admin(count for patient by date and doctor list)

router.get('/get/admin/count',verifyToken,function(req,res){
    Users. findAndCountAll({where:{ is_active:'1',is_doctor:'1' }}).then(function(users){ 
        var response_array = [];
        var today = new Date().toLocaleDateString();
        var count = 0;
        Patient_personal_details.findAll({where:{is_active:'1'}}).then(function(patient_data)
        {
            var processItems = function(x)
            {
                if( x < patient_data.length )
                {
                    var date = moment(patient_data[x].createdAt).format("MM/DD/YYYY");
                    var current_date = Date.parse(date);
                    var today_date = Date.parse(today);
                    if(current_date == today_date)
                    {
                        count++;
                    }
                    processItems(x+1);
                }
               
            }
            processItems(0)
            let payload ={}
            payload.doctor_count = users.count;
            payload.patient_count = count
            res.status(200).send(payload)
            
        })

    })

})
// super admin

// count for SuperAdmin(count for all patient and doctor list)

router.get('/superadmin/count',verifyToken,function(req,res){
    Users.findAndCountAll({where:{ is_active:'1',is_doctor:'1'}}).then(function(Users){
      Patient_personal_details.findAndCountAll({where:{ is_active:'1'}}).then(function( Patient_data){
        let payload = {}
        payload.doctor_count = Users.count
        payload.patient_count = Patient_data.count
        res.status(200).send(payload)
     })
   })
  });



  // search method
router.get('/search',function(req,res){
    var request=req.body;
    var find=req.body.find;
    Users.findAll({where:{[Op.or]:{user_name: {[Op.like]:'%'+find+'%'},mobile_number: {[Op.like]:'%'+find+'%'}}
    }}).then(function(Users){
    return res.send(Users)
    })
  })



  //Get Patient List
router.post('/patient/search/today',verifyToken,function(req,res)
{
	var today = new Date().toLocaleDateString();
    var response_array = [];
    var request=req.body;
    var find=req.body.find;
    Patient_personal_details.findAll({where:{[Op.or]:{first_name: {[Op.like]:'%'+find+'%'},mobile_number: {[Op.like]:'%'+find+'%'}}
,is_active:1},order:[['id','DESC']]}).then(function(patient_basic)
    {

     	var patient_payload = patient_basic;
        var processItems = function(x)
        {
            if( x < patient_payload.length) 
            {
            	Patient_medical_history.findOne({where:{patient_id_fk:patient_payload[x].id}}).then(function(patient_data)
            	{
                	var data = {...patient_payload[x].dataValues,...patient_data.dataValues}
            		Patient_address_details.findOne({where:{patient_id_fk:patient_payload[x].id}}).then(function(patient_medical)
            		{
                        var additional_data ={...data,...patient_medical.dataValues}
                        Patient_doctor_mapping.findOne({where:{patient_id_fk:patient_payload[x].id}}).then(function(map_data){
                        	Users.findOne({where:{id:map_data.user_id_fk}}).then(function(user_data)
                        	{
                        		var user = 
                        		{
                        			"user_name" : user_data.dataValues.user_name
                        		}
                        		var final_data = {...additional_data,...user}
	                			var date = moment(final_data.createdAt).format("MM/DD/YYYY");
	                			var current_date = Date.parse(date);
                                var today_date = Date.parse(today);
                                console.log(current_date)
                                console.log(today_date)
                        	    if(today_date == current_date)
                        	    {
	                			  response_array.push(final_data)
	                		    }
	                			processItems(x+1);
                	    	})
                        })
                        
            		})
        		})
            }
            else 
            {
          		res.status(200).send({"patient_data":response_array})
        	}
    	}
    	processItems(0)
  	})
});

  /// working apii........................................................
//Add New patient
router.post('/new/validation',verifyToken,function(req,res)
{
  //Get token from the header
  var token   = req.headers.authorization;
  //Decode the token into json object
  var decoded = jwt_decode(token);
  const {email_id,mobile_number,gender,}=req.body;
  var request = req.body;
  let errors =[];
  
  Patient_personal_details.findOne({where:{email_id:email_id}}).then(details=>{
   // console.log(user)

    if(details){
      errors.push({msg:"already exist"});
      res.send("user email_id already exist")
    }
    else {
      
        Patient_personal_details.findOne({where:{mobile_number:mobile_number}}).then(number=>{
        if(number){
          errors.push({msg:"already exist"});
          res.send("user mobile_number already exist")
        }
        else{
            var created = 
            {
                 patient_id_number : "PAT"+Math.floor(10000 + Math.random() * 90000),
                 createdby : decoded.id
            }
            var request = {...created,...req.body};
         
            Patient_personal_details.create(request).then(function(patient_data){
                var patient_medical_payload = 
                {
                    patient_id_fk     : patient_data.id,
                    height            : request.height,
                    weight            : request.weight,
                    sugar_level       : request.sugar_level,
                    bp_level          : request.bp_level,
                    heamoglobin_level : request.heamoglobin_level,
                    surgeries_in_past : request.surgeries_in_past,
                    allergies         : request.allergies,
                    appointment_date  : request.appointment_date,
                    createdby         : patient_data.createdby
                    
                }
                Patient_medical_history.create(patient_medical_payload).then(function(patient_medical)
                {
                    var patient_address_payload = 
                    {
                        patient_id_fk    : patient_data.id,
                        door_no          : request.door_no,
                        street_name      : request.street_name,
                        city_name        : request.city_name,
                        state_name       : request.state_name,
                        landmark         : request.landmark,
                           description   : request.description,
                        createdby     : patient_data.createdby
                        
                    }
                    Patient_address_details.create(patient_address_payload).then(function(patient_address)
                    {
                        var mapping_payload = 
                        {
                            "patient_id_fk" : patient_data.id,
                            "user_id_fk"    : request.user_id,
                            "createdby"     : patient_data.createdby
                            
                        }
                        Patient_doctor_mapping.create(mapping_payload).then(function(patient_mapping)
                        {
                            res.send("Patient created successfully");
                        })
                    })
                })
            })
        }
      })
 

    }
  })
})


// patient report upload
router.post('/patient/report',upload.single("image"),verifyToken,function (req, res)
{
    //console.log(req);
    //Get token from the header
    var token   = req.headers.authorization;
    //Decode the token into json objects
    var decoded = jwt_decode(token);
    //merge two json objects
    var created =
    {
      patient_id_fk:req.id,
      createdby : decoded.id,
      "report":url+req.file.filename }
    var request = {...created,...req.body};
    //console.log("file",req.file);
  patient_report_details.create(request).then(function(user_image,err)
   {
          res.status(200).send("image upload success");
        })
,function(err){
    if(err && err.errors[0].message)
    {
      return res.status(500).send(err.errors[0].message)
    }
    res.status(500).send("image upload filed");
  }
});

// patient report get by id
router.get('/:id',verifyToken,function(req,res)
{
  patient_report_details.findAll({where: {id: req.params.id}}).then(function(patient_report)
  {
    return res.send(patient_report);
  })
});

// update report for patient
router.put('/report/update/:patient_id_fk',upload.single("image"),verifyToken,function(req,res)
{
  var patient_id_fk = req.params.patient_id_fk;
  // var user_id_fk = decoded.id
  var token   = req.headers.authorization;
  var decoded = jwt_decode(token);
  var updated =
  {
    updatedby : decoded.id,
    "report":url+req.file.originalname
  }
  var request = {...updated,...req.body};
  patient_report_details.update(request,{where:{patient_id_fk:patient_id_fk}}).then(function(patiennt_report){
    {
      res.send("user_image updated successfully");
    }
  })
});

module.exports = router;



//............................................................................................................
//Add New patient alt for either mobile number nor email_id
router.post('/new/val/pat',verifyToken,function(req,res)
{
  //Get token from the header
  var token   = req.headers.authorization;
  //Decode the token into json object
  var decoded = jwt_decode(token);
  const {email_id,mobile_number,gender,}=req.body;
  var request = req.body;
  let errors =[];

if(req.body.email_id){
  Patient_personal_details.findOne({where:{email_id:email_id,is_active:1}}).then(details=>{
    // console.log(user)
 
     if(details){
       errors.push({msg:"already exist"});
       res.send("user email_id already exist")
     }
     else {
       
         Patient_personal_details.findOne({where:{mobile_number:mobile_number,is_active:1}}).then(number=>{
         if(number){
           errors.push({msg:"already exist"});
           res.send("user mobile_number already exist")
         }
         else{
             var created = 
             {
                  patient_id_number : "PAT"+Math.floor(10000 + Math.random() * 90000),
                  createdby : decoded.id
             }
             var request = {...created,...req.body};
          
             Patient_personal_details.create(request).then(function(patient_data){
                 var patient_medical_payload = 
                 {
                     patient_id_fk     : patient_data.id,
                     height            : request.height,
                     weight            : request.weight,
                     sugar_level       : request.sugar_level,
                     sugar_choose      :request.sugar_choose,
                     bp_level          : request.bp_level,
                     bp_choose         :request.bp_choose,
                     heamoglobin_level : request.heamoglobin_level,
                     heamoglobin_choose:request.heamoglobin_choose,
                     surgeries_in_past : request.surgeries_in_past,
                     surgeries_choose  :request.surgeries_choose,
                     allergies         : request.allergies,
                     allergies_choose  :request.allergies_choose,
                     createdby         : patient_data.createdby
                     
                 }
                 Patient_medical_history.create(patient_medical_payload).then(function(patient_medical)
                 {
                     var patient_address_payload = 
                     {
                         patient_id_fk    : patient_data.id,
                         door_no          : request.door_no,
                         street_name      : request.street_name,
                         city_name        : request.city_name,
                         state_name       : request.state_name,
                         landmark         : request.landmark,
                         description   : request.description,
                         createdby     : patient_data.createdby
                         
                     }
                     Patient_address_details.create(patient_address_payload).then(function(patient_address)
                     {
                         var mapping_payload = 
                         {
                             "patient_id_fk" : patient_data.id,
                             "user_id_fk"    : request.user_id,
                             "createdby"     : patient_data.createdby
                             
                         }
                         Patient_doctor_mapping.create(mapping_payload).then(function(patient_mapping)
                         {
                             res.send("Patient created successfully");
                         })
                     })
                 })
             })
         }
       })
  
 
     }
   
     })
}else{
  Patient_personal_details.findOne({where:{mobile_number:mobile_number}}).then(number=>{
    if(number){
      errors.push({msg:"already exist"});
      res.send("user mobile_number already exist")
    }
    else{
        var created = 
        {
             patient_id_number : "PAT"+Math.floor(10000 + Math.random() * 90000),
             createdby : decoded.id
        }
        var request = {...created,...req.body};
     
        Patient_personal_details.create(request).then(function(patient_data){
            var patient_medical_payload = 
            {
                patient_id_fk     : patient_data.id,
                height            : request.height,
                weight            : request.weight,
                sugar_level       : request.sugar_level,
                sugar_choose      :request.sugar_choose,
                bp_level          : request.bp_level,
                bp_choose         :request.bp_choose,
                heamoglobin_level : request.heamoglobin_level,
                heamoglobin_choose:request.heamoglobin_choose,
                surgeries_in_past : request.surgeries_in_past,
                surgeries_choose  :request.surgeries_choose,
                allergies         : request.allergies,
                allergies_choose  :request.allergies_choose,
                createdby         : patient_data.createdby
                
            }
            Patient_medical_history.create(patient_medical_payload).then(function(patient_medical)
            {
                var patient_address_payload = 
                {
                    patient_id_fk    : patient_data.id,
                    door_no          : request.door_no,
                    street_name      : request.street_name,
                    city_name        : request.city_name,
                    state_name       : request.state_name,
                    landmark         : request.landmark,
                   description   : request.description,
                    createdby     : patient_data.createdby
                    
                }
                Patient_address_details.create(patient_address_payload).then(function(patient_address)
                {
                    var mapping_payload = 
                    {
                        "patient_id_fk" : patient_data.id,
                        "user_id_fk"    : request.user_id,
                        "createdby"     : patient_data.createdby
                        
                    }
                    Patient_doctor_mapping.create(mapping_payload).then(function(patient_mapping)
                    {
                        res.send("Patient created successfully");
                    })
                })
            })
        })
    }
  })
}

  
})
/// past 30days record get
router.get('/get/past/count',verifyToken,function(req,res){
  Patient_personal_details.findAndCountAll({where:{ is_active:'1',createdAt:{[Op.gte]: moment().subtract(30,'days').toDate() }}}).then(function(Users)
  {
    res.send(Users)
  })
  
})

/// dash board for admin page
router.get('/get/active/count',verifyToken,function(req,res){
 
  var response_array = [];
  var today = new Date().toLocaleDateString();
  var count = 0;
  Appointment_doctor.findAll({where:{is_active:'1'}}).then(function(patient_data)
  {
      var processItems = function(x)
      {
          if( x < patient_data.length )
          {
              var date = moment(patient_data[x].appointment_date).format("MM/DD/YYYY");
              var current_date = Date.parse(date);
              var today_date = Date.parse(today);
              if(current_date == today_date)
              {
                  count++;
              }
              processItems(x+1);
           }
       
      }
     
      processItems(0)
      let payload ={}
     payload.active_count = count;
     res.status(200).send(payload)
      
  })
})

/// dash board for admin page
router.get('/get/inactive/count',verifyToken,function(req,res){
 
  var response_array = [];
  var today = new Date().toLocaleDateString();
  var count = 0;
  Appointment_doctor.findAll({where:{is_active:'0'}}).then(function(patient_data)
  {
      var processItems = function(x)
      {
          if( x < patient_data.length )
          {
              var date = moment(patient_data[x].appointment_date).format("MM/DD/YYYY");
              var current_date = Date.parse(date);
              var today_date = Date.parse(today);
              if(current_date == today_date)
              {
                  count++;
              }
              processItems(x+1);
           }
       
      }
     
      processItems(0)
      let payload ={}
     payload.cancelled_count = count;
     res.status(200).send(payload)
      
  })
})

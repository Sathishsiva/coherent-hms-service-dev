var express       = require('express'),
    router        = express.Router(),
    editJsonFile  = require('edit-json-file')
    bodyParser    = require('body-parser');
const jwt = require('jsonwebtoken');
const jwt_decode = require('jwt-decode');
var verifyToken    = require('../verifyToken.js');
var config = editJsonFile(__root +"config.json");
const moment= require('moment');
const date = require('date-and-time');
const  Sequelize = require('sequelize');
const Op = Sequelize.Op


router.use(bodyParser.json());

//Declare the database tables
var prescription = __db.prescription;
var Patient_medical_history = __db.Patient_medical_history;
var Patient_personal_details = __db.Patient_personal_details;
var Patient_address_details = __db.Patient_address_details;
var Patient_doctor_mapping = __db.Patient_doctor_mapping;
var Users = __db.Users;
var Appointment_doctor = __db.Appointment_doctor;


// get by patient_id_fk  prescription 
router.get('/:patient_id_fk',verifyToken,function (req,res) { 
    prescription.findAll({where: {patient_id_fk: req.params.patient_id_fk}}).then(function(prescription){
    res.send(prescription);  
  })
});


// post prescription
router.post('/add',verifyToken,function (req, res) 
{  
  //Get token from the header
  var token   = req.headers.authorization;
  //Decode the token into json object
  var decoded = jwt_decode(token);
  //merge two json objects
  var created = { "createdby" : decoded.id }
  var created = { "user_id_fk" : decoded.id }
   var Master= req.body
   
  for(let i =0; i<Master.length;i++){
      Master[i].createdby = decoded.id;
      Master[i].user_id_fk = decoded.id;
      prescription.create(Master[i]) 
  }
    res.status(200).send(" created successfully")
});

// get doctor's patient list by decoded id
//////
router.get('/patient/list',verifyToken,function(req,res)
{
  //Get token from the header
  var token   = req.headers.authorization;
  //Decode the token into json object
  var decoded = jwt_decode(token);
  //merge two json objects
  var created = { "createdby" : decoded.id }
 // console.log(created)
	var today = new Date().toLocaleDateString();
    var response_array = [];
    var patient_details;
    Patient_doctor_mapping.findAll({where:{user_id_fk:decoded.id,is_active:'1'},attributes:['patient_id_fk']}).then(function(patient_map)
    {
    var processItems = function(x)
     {
    if( x < patient_map.length )
     {
         
    Patient_personal_details.findOne({where:{id:patient_map[x].patient_id_fk}}).then(function(response)
      {
        Patient_medical_history.findOne({where:{patient_id_fk:patient_map[x].patient_id_fk}}) .then(function(res)
       {
        Patient_address_details.findOne({where:{patient_id_fk:patient_map[x].patient_id_fk}}) .then(function(address)
              {
              var data = {...patient_map.dataValues,...response.dataValues,...res.dataValues,...address.dataValues}
              var date = moment(data.createdAt).format("MM/DD/YYYY");
	                			var current_date = Date.parse(date);
                                var today_date = Date.parse(today);
                               // console.log(current_date)
                              //  console.log(today_date)
                        	    if(today_date == current_date)
              response_array.push(data)
              processItems(x+1);
              })
            })
          })
        }
        else 
        {
          res.status(200).send({"patient_data": response_array})
        }
      }
      processItems(0)
    })
   
  })

  // update for the patient medical history
    
router.put('/update/:id',verifyToken,function(req,res)
{
    var id = req.params.id;
    //Get token from the header
    var token   = req.headers.authorization;
    //Decode the token into json object
    var decoded = jwt_decode(token);
    var updated = { "updatedby" : decoded.id }
    var request = {...req.body,...updated}
    Patient_medical_history.update(request,{where:{id:id}}).then(function(user_update)
    {
    res.send("patient history updated successfully.");
   })
  })
  

  // get patient list starting date to ending date by doctor decoded id
  ////// between dates
router.get('/patient/between/date',verifyToken,function(req,res)
{
  //Get token from the header
  var token   = req.headers.authorization;
  //Decode the token into json object
  var decoded = jwt_decode(token);
  //merge two json objects
  var created = { "createdby" : decoded.id }
  console.log(created)
	
    var response_array = [];
    var patient_details;
    Patient_doctor_mapping.findAll({where:{user_id_fk:decoded.id},attributes:['patient_id_fk']}).then(function(patient_map)
    {
      var processItems = function(x)
      {
        if( x < patient_map.length )
        {
         
        Patient_personal_details.findOne({where:{id:patient_map[x].patient_id_fk}}).then(function(response)
          {
            Patient_medical_history.findOne({where:{patient_id_fk:patient_map[x].patient_id_fk}}) .then(function(res)
            {
              Patient_address_details.findOne({where:{patient_id_fk:patient_map[x].patient_id_fk}}) .then(function(address)
              {
                prescription.findAll({where:{patient_id_fk:patient_map[x].patient_id_fk}}) .then(function(prescription)
                 {
                   console.log(prescription)
                 var data = {...patient_map.dataValues,...response.dataValues,...res.dataValues,...address.dataValues}
                 data.prescription =prescription;
                 var current_date = data.createdAt.toLocaleDateString();
                 var date = moment(current_date).format("MM/DD/YYYY");
         var from_date = Date.parse(req.body.start_date);
                         var to_date = Date.parse(req.body.end_date);
                         var check_date = Date.parse(date);
                        if(check_date <= to_date && check_date >= from_date)  
              response_array.push(data)
              processItems(x+1);
                 })
            })
            })
          })
        }
        else 
        {
          res.status(200).send({"patient_data": response_array})
        }
      }
      processItems(0)
    })
   
  })

  // get  mobile_number or  patient_id_number 
  router.get('/patient/search',verifyToken,function(req,res)
{ 
  
//var mobile_number= req.body.mobile_number
 
  //   var patient_id_number =req.body.patient_id_number
if( req.body.patient_id_number)
{
    Patient_personal_details.findOne({ where: {patient_id_number :req.body.patient_id_number}}).then(function(details)
   {
        Patient_medical_history.findOne({where:{patient_id_fk:details.id}}).then(function(Patient_data)
        {
            Patient_address_details.findOne({where:{patient_id_fk:details.id}}).then(function(Patient_details)
            {
                Patient_doctor_mapping.findOne({where:{patient_id_fk:details.id}}).then(function(Patient_mappings)
                {
                  prescription.findAll({where:{patient_id_fk:details.id}}).then(function(prescription)
                    {
                        var data ={...details.dataValues,...Patient_data.dataValues,...Patient_details.dataValues,...Patient_mappings.dataValues}
                        data.prescription =prescription;
                        return res.send(data);
                    })
                    
                })
            })
        })
    })
}else {
  
  Patient_personal_details.findOne({ where: {mobile_number :req.body.mobile_number}}).then(function(details)
  {
       Patient_medical_history.findOne({where:{patient_id_fk:details.id}}).then(function(Patient_data)
       {
           Patient_address_details.findOne({where:{patient_id_fk:details.id}}).then(function(Patient_details)
           {
               Patient_doctor_mapping.findOne({where:{patient_id_fk:details.id}}).then(function(Patient_mappings)
               {
                 prescription.findAll({where:{patient_id_fk:details.id}}).then(function(prescription)
                   {
                       var data ={...details.dataValues,...Patient_data.dataValues,...Patient_details.dataValues,...Patient_mappings.dataValues}
                       data.prescription =prescription;
                       return res.send(data);
                   })
                   
               })
           })
       })
   })
 


}
  });


// between dates for patient list for doctor logging extra
  
  router.post('/patient/between/date/doctor',verifyToken,function(req,res)
  {
    //Get token from the header
    var token   = req.headers.authorization;
    //Decode the token into json object
    var decoded = jwt_decode(token);
    //merge two json objects
    var created = { "createdby" : decoded.id }
    console.log(created)
    
      var response_array = [];
      var patient_details;
      Patient_doctor_mapping.findAll({where:{user_id_fk:decoded.id},attributes:['patient_id_fk']}).then(function(patient_map)
      {
        var processItems = function(x)
        {
          if( x < patient_map.length )
          {
           
          Patient_personal_details.findOne({where:{id:patient_map[x].patient_id_fk}}).then(function(response)
            {
              Patient_medical_history.findOne({where:{patient_id_fk:patient_map[x].patient_id_fk}}) .then(function(res)
              {
                Patient_address_details.findOne({where:{patient_id_fk:patient_map[x].patient_id_fk}}) .then(function(address)
                {
                  prescription.findAll({where:{patient_id_fk:patient_map[x].patient_id_fk}}) .then(function(prescription)
                   {
                     console.log(prescription)
                   var data = {...patient_map.dataValues,...response.dataValues,...res.dataValues,...address.dataValues}
                   data.prescription =prescription;
                   var current_date = data.createdAt.toLocaleDateString();
                   var date = moment(current_date).format("YYYY/MM/DD");
           var from_date = Date.parse(req.body.start_date);
                           var to_date = Date.parse(req.body.end_date);
                           var check_date = Date.parse(date);
                          if(check_date <= to_date && check_date >= from_date)  
                response_array.push(data)
                processItems(x+1);
                   })
              })
              })
            })
          }
          else 
          {
            res.status(200).send({"patient_data": response_array})
          }
        }
        processItems(0)
      })
     
    })


    ///patient_id_number extreaaa
    // addtional
    
  router.post('/patient/doctor/search',verifyToken,function(req,res)
  { 
    
  //var mobile_number= req.body.mobile_number
   
    //   var patient_id_number =req.body.patient_id_number
  if( req.body.patient_id_number)
  {
      Patient_personal_details.findOne({ where: {patient_id_number :req.body.patient_id_number}}).then(function(details)
     {
          Patient_medical_history.findOne({where:{patient_id_fk:details.id}}).then(function(Patient_data)
          {
              Patient_address_details.findOne({where:{patient_id_fk:details.id}}).then(function(Patient_details)
              {
                  Patient_doctor_mapping.findOne({where:{patient_id_fk:details.id}}).then(function(Patient_mappings)
                  {
                    prescription.findAll({where:{patient_id_fk:details.id}}).then(function(prescription)
                      {
                          var data ={...details.dataValues,...Patient_data.dataValues,...Patient_details.dataValues,...Patient_mappings.dataValues}
                          data.prescription =prescription;
                          return res.send(data);
                      })
                      
                  })
              })
          })
      })
  }else {
    
    Patient_personal_details.findOne({ where: {mobile_number :req.body.mobile_number}}).then(function(details)
    {
         Patient_medical_history.findOne({where:{patient_id_fk:details.id}}).then(function(Patient_data)
         {
             Patient_address_details.findOne({where:{patient_id_fk:details.id}}).then(function(Patient_details)
             {
                 Patient_doctor_mapping.findOne({where:{patient_id_fk:details.id}}).then(function(Patient_mappings)
                 {
                   prescription.findAll({where:{patient_id_fk:details.id}}).then(function(prescription)
                     {
                         var data ={...details.dataValues,...Patient_data.dataValues,...Patient_details.dataValues,...Patient_mappings.dataValues}
                         data.prescription =prescription;
                         return res.send(data);
                     })
                     
                 })
             })
         })
     })
   
  
  
  }
    });




    //////
router.post('/patient/list/search',verifyToken,function(req,res)
{
  //Get token from the header
  var token   = req.headers.authorization;
  //Decode the token into json object
  var decoded = jwt_decode(token);
  //merge two json objects
  var created = { "createdby" : decoded.id }
 // console.log(created)
	var today = new Date().toLocaleDateString();
    var response_array = [];
    var request=req.body;
    var find=req.body.find;
    var patient_details;
    Patient_doctor_mapping.findAll({where:{user_id_fk:decoded.id,is_active:'1'},attributes:['patient_id_fk']}).then(function(patient_map)
    {      //{[Op.or]:{user_name: {[Op.like]:'%'+find+'%'},mobile_number: {[Op.like]:'%'+find+'%'}} },order:[['id','DESC']],
    var processItems = function(x)
     {
    if( x < patient_map.length )
     {
         
    Patient_personal_details.findOne({where:{[Op.or]:{first_name: {[Op.like]:'%'+find+'%'},mobile_number: {[Op.like]:'%'+find+'%'}},id:patient_map[x].patient_id_fk},order:[['id','DESC']]}).then(function(response)
      {
        Patient_medical_history.findOne({where:{patient_id_fk:patient_map[x].patient_id_fk}}) .then(function(res)
       {
        Patient_address_details.findOne({where:{patient_id_fk:patient_map[x].patient_id_fk}}) .then(function(address)
              {
              var data = {...patient_map.dataValues,...response.dataValues,...res.dataValues,...address.dataValues}
              var date = moment(data.createdAt).format("MM/DD/YYYY");
	                			var current_date = Date.parse(date);
                                var today_date = Date.parse(today);
                               // console.log(current_date)
                              //  console.log(today_date)
                        	    if(today_date == current_date)
              response_array.push(data)
              processItems(x+1);
              })
            })
          })
        }
        else 
        {
          res.status(200).send({"patient_data": response_array})
        }
      }
      processItems(0)
    })
   
  })

  ///appointment date for dashboard count using appointment table
  router.get('/appointment/count/',verifyToken,function(req,res)
            {
              //Get token from the header
              var token   = req.headers.authorization;
              //Decode the token into json object
              var decoded = jwt_decode(token);
              var response_array = [];
               var today = new Date().toLocaleDateString();
              var count = 0;
              Appointment_doctor.findAll({where:{user_id_fk:decoded.id,is_active:'1'}}).then(function(patient_map)
              {
                var processItems = function(x)
            {
                if( x < patient_map.length )
                {
                    var date = moment(patient_map[x].appointment_date).format("MM/DD/YYYY");
                    var current_date = Date.parse(date);
                    var today_date = Date.parse(today);
                    if(current_date == today_date)
                    {
                        count++;
                    }
                    processItems(x+1);
                }
               
            }
            processItems(0)
            let payload ={}
            payload.today_appointment= count
            res.status(200).send(payload)
              })
            })

            ///total count of patient for dashboard for the specfic doctor
           router.get('/total/count/',verifyToken,function(req,res)
           {
             //Get token from the header
             var token   = req.headers.authorization;
             //Decode the token into json object
             var decoded = jwt_decode(token);
             var response_array = [];
             var today = new Date().toLocaleDateString();
            var count = 0;
             Patient_doctor_mapping.findAndCountAll({where:{user_id_fk:decoded.id,is_active:'1'}}).then(function(map)
             {
             
             Patient_doctor_mapping.findAll({where:{user_id_fk:decoded.id,is_active:'1'}}).then(function(patient_map)
             {
               var processItems = function(x)
           {
               if( x < patient_map.length )
               {
                   var date = moment(patient_map[x].createdAt).format("MM/DD/YYYY");
                   var current_date = Date.parse(date);
                   var today_date = Date.parse(today);
                   if(current_date == today_date)
                   {
                       count++;
                   }
                   processItems(x+1);
               }
              
           }
           processItems(0)
           let payload ={}
           payload.today_count = count
           payload.total_patient_count = map.count
           res.status(200).send(payload)
             })
            })
            }) 

       

    /// past 30days record get for doctor
router.get('/get/past/count',verifyToken,function(req,res){

  //Get token from the header
  var token   = req.headers.authorization;
  //Decode the token into json object
  var decoded = jwt_decode(token);
  Patient_doctor_mapping.findAndCountAll({where:{ user_id_fk:decoded.id,is_active:'1',createdAt:{[Op.gte]: moment().subtract(30,'days').toDate() }}}).then(function(Users)
  {
    res.send(Users)
  })
  
})

module.exports = router;
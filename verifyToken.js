const jwt = require('jsonwebtoken');
var editJsonFile  = require('edit-json-file');
var config = editJsonFile(__root +"config.json");

function verifyToken(req, res, next) 
{
  var token = req.headers.authorization;
  var secret = config.get('secret');
  if (!token) 
  {
    return res.status(403).send({ auth: false, message: 'No token provided.' });
  }
  //Token validation
  var jwttoken = token.split(" ")[1];
  jwt.verify(jwttoken,secret, function(err,decoded) 
  {
    if (err) 
    {
      console.log(err);
      return res.status(401).send({ auth: false, message: 'Failed to authenticate token.' });    
    }
    else
    {
      next();
    }
  });
}

module.exports = verifyToken;
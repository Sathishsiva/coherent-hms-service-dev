var express       = require('express'),
    router        = express.Router(),
    bodyParser    = require('body-parser');
var verifyToken    = require('../verifyToken.js');
const jwt_decode = require('jwt-decode');
const  Sequelize = require('sequelize');
const Op = Sequelize.Op

var Appointment_doctor = __db.Appointment_doctor;

const moment= require('moment');
const date = require('date-and-time');

router.use(bodyParser.json());

//Add Appointment
router.post('/new',verifyToken,function (req, res) 
{  
  //Get token from the header
  var token   = req.headers.authorization;
  //Decode the token into json object
  var decoded = jwt_decode(token);
  //merge two json objects
  var created = { "createdby" : decoded.id }
  var request = {...created,...req.body};
	Appointment_doctor.create(request).then(function(Appointment_doctor)
  {
    res.status(200).send("Appointment_doctor created successfully")
	})
});

//Get Appointment List
router.get('/',verifyToken,function(req,res)
{
  Appointment_doctor.findAll({where:{is_active:'1'},order:[['id','DESC']]}).then(function(Appointment_doctor)
  {
    return res.send(Appointment_doctor);
  })
});



//Update the Appointment data
router.put('/update/data/:id',verifyToken,function(req,res) 
{
  var id = req.params.id;
  //Get token from the header
  var token   = req.headers.authorization;
  //Decode the token into json object
  var decoded = jwt_decode(token);
  var updated = { "updatedby" : decoded.id }
  var request = {...updated,...req.body};
  Appointment_doctor.update(request,{where:{id:id}}).then(function(Appointment_doctor)
  {
    if (Appointment_doctor == 1) 
    {
      res.send("Appointment_doctor updated successfully");
    } 
  })
});


//Appointment count using date
//get appointment count
router.get('/get/count/Appointment',verifyToken,function(req,res){
  var response_array = [];
  var today = new Date().toLocaleDateString();
  var count = 0;
  Appointment_doctor.findAll({where:{is_active:'1'}}).then(function(patient_data)
  		{
    		var processItems = function(x)
   			{
        	if( x < patient_data.length )
        	{
            var date = moment(patient_data[x].appointment_date).format("MM/DD/YYYY");           
            var current_date = Date.parse(date);
            var today_date = Date.parse(today);
            if(current_date == today_date)
            {
                count++;
            }
            processItems(x+1);
        }
   	 }
    	processItems(0)
    	res.status(200).send({"count":count})
  	})
});

// get by id for appointment
router.get('/:id',verifyToken,function(req,res)
{
  Appointment_doctor.findAll({where: {id: req.params.id}}).then(function(Appointment_doctor)
  {
    return res.send(Appointment_doctor);
  })
});

//Deactivate the appointment
router.put('/deactivate/:id',verifyToken,function(req, res) 
{  
	var id = req.params.id;
  //Get token from the header
  var token   = req.headers.authorization;
  //Decode the token into json object
  var decoded = jwt_decode(token);
  Appointment_doctor.update({is_active:'0',updatedby:decoded.id},{where:{id:id}}).then(function(Appointment_doctor) 
  {
    if(Appointment_doctor > 0) 
    {
      res.send("The Appointment Id: "+id+" is deactivated successfully")
    }
	})
});

//Get test
router.get('/test', function (req, res) {
  res.send('HI Node TEAM!');
});

// Appointment search List
router.post('/get/search',verifyToken,function(req,res)
{
  var request=req.body;
  var find=req.body.find;
  Appointment_doctor.findAll({where:{[Op.or]:{patient_name: {[Op.like]:'%'+find+'%'},mobile_number: {[Op.like]:'%'+find+'%'}}
  ,is_active:1},order:[['id','DESC']]}).then(function(Appointment_doctor)
  {
    return res.send(Appointment_doctor);
  })
});



module.exports = router;
var express       = require('express'),
    router        = express.Router(),
    editJsonFile  = require('edit-json-file'),
    bodyParser    = require('body-parser');
const jwt = require('jsonwebtoken');
var verifyToken    = require('../verifyToken.js')
var config = editJsonFile(__root +"config.json");
const jwt_decode = require('jwt-decode');
const merge = require('deepmerge')
//const errors = validationResult(req);
const moment= require('moment');
var sendMail = require('../sendMail.js');
const date = require('date-and-time');
const multer = require('multer');
const  Sequelize = require('sequelize');
const Op = Sequelize.Op

const url = 'http://65.108.240.252/profile/images/'
//const { where } = require('sequelize/types');

// Storage Engin That Tells/Configures Multer for where (destination) and how (filename) to save/upload our files
const fileStorageEngine = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, "/data/www/profile/images"); //important this is a direct path fron our current file to storage location
  },
  filename: (req, file, cb) => {
    cb(null,Date.now()+'_' +file.originalname);
  },
})

const fileFilter = (req, file, cb) => {
  // reject a file
  if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png'|| file.mimetype === 'image/jpg') {
    cb(null, true);
  } else {
    cb(null, false);
  }
};
// The Multer Middleware that is passed to routes that will receive income requests with file data (multipart/formdata)
// You can create multiple middleware each with a different storage engine config so save different files in different locations on server
const upload = multer({ storage: fileStorageEngine, limits: {
  fileSize: 1024 * 1024 * 5
},
   fileFilter: fileFilter 
});

var Users = __db.Users; 
var Roles = __db.Roles;
var User_role_mapping = __db.User_role_mapping;
var Master_specialists = __db.Master_specialists;
var Doctor_specialist_mapping  = __db.Doctor_specialist_mapping;
var user_personal_details = __db.user_personal_details;
var user_image_mapping=__db.user_image_mapping
var Patient_medical_history = __db.Patient_medical_history;
var Patient_personal_details = __db.Patient_personal_details;
var Patient_address_details = __db.Patient_address_details;
var Patient_doctor_mapping = __db.Patient_doctor_mapping;
var User_validation =__db.User_validation;

router.use(bodyParser.json());


//Add New Users
router.post('/new',verifyToken,function(req,res)
{
  //Get token from the header
  var token   = req.headers.authorization;
  //Decode the token into json object
  var decoded = jwt_decode(token);
  var request = req.body;
  var random_number = 
  {
    "user_random_number" : "USR"+Math.floor(100000 + Math.random() * 900000),
    "createdby" : decoded.id
  }
  //merge two json objects
  var user_data_payload = {...request,...random_number};
  Users.create(user_data_payload).then(function(data)
  {
    if (data.is_doctor == '1')
    {
      var doctor_payload =
      {
        "user_id_fk" : data.id,
        "specialist_id_fk"   : req.body.specialist_id,
        "createdby" : data.createdby,
        "updatedby" : data.updatedby
      }
      Doctor_specialist_mapping.create(doctor_payload).then(function(doctor_data){});
      //Mapping the roles for Doctor
      Roles.findOne({where:{role_name:"Doctor"}}).then(function(role_data)
      {
        var map_data = 
        {
          "role_id_fk" : role_data.id,
          "user_id_fk" : data.id,
          "createdby"  : data.createdby,
          "updatedby"  : data.updatedby
        }
        User_role_mapping.create(map_data).then(function(role_map)
        {
          res.send("User_doctor created successfully");
        });
      })  
    }
    else
    {
      //Mapping the role for Admin
      Roles.findOne({where:{role_name:"Admin"}}).then(function(role_data)
      {
        var map_data = 
        {
          "role_id_fk" : role_data.id,
          "user_id_fk" : data.id,
          "createdby"  : data.createdby,
          "updatedby"  : data.updatedby
        }
        User_role_mapping.create(map_data).then(function(role_map)
        {
          res.send("User_admin created successfully");
        });
      }) 
    }
  })
});


//Get Doctor List
router.get('/role/doctor/specialist',verifyToken,function(req,res)
{
  var response_array = [];
  Users.findAll({where:{is_doctor:'1',is_active:'1'},order:[['id','DESC']]}).then(function(user)
  {
    var processItems = function(x)
    {
      if( x < user.length )
      {
        Doctor_specialist_mapping.findOne({where:{user_id_fk:user[x].id},attributes:['specialist_id_fk']}).then(function(response)
        {
          Master_specialists.findOne({where:{id:response.specialist_id_fk},attributes:['speciallation_name']}) .then(function(res)
          {
            var data = 
            {
              "id": user[x].id,
              "fullname" : user[x].full_name,
              "emailid" :user[x].email_id,
              "mobile_number" : user[x].mobile_number,
              "gender": user[x].gender,
              "education": user[x].education,
              "experience":user[x].experience,
              "speciallation_name" :res.speciallation_name
            }
            response_array.push(data)
            processItems(x+1);
          })
        })
      }
      else 
      {
        res.status(200).send({"user_data": response_array})
      }
    }
    processItems(0)
  })
});

//Get Admin list
router.get('/role/admin',verifyToken,function(req,res)
{
  Users.findAll({where: {is_doctor:'0',is_active:'1'},order:[['id','DESC']]}).then(function(Users)
  {
    return res.send(Users);
  })
});

//Get all Users
router.get('/',verifyToken,function(req,res)
{
  Users.findAll({order:[['id','DESC']]}).then(function(Users)             ///order desending order
  {
    return res.send(Users);
  })
});


//Deactivate the User
router.put('/deactivate/:id',function (req, res) 
{  
  //Get token from the header
  var token   = req.headers.authorization;
  //Decode the token into json object
  var decoded = jwt_decode(token);
  Users.update({is_active:'0',updatedby:decoded.id},{where:{id:req.params.id}}).then(function(user_update)
  {
    Doctor_specialist_mapping.update({is_active:'0',updatedby:decoded.id},{where:{user_id_fk:req.params.id}}).then(function(map_data){})
    
      User_role_mapping.update({is_active:'0',updatedby:decoded.id},{where:{user_id_fk:req.params.id}}).then(function(role_map){})
    if(user_update > 0) 
    {
      res.status(200).send("User deactivated successfully")
    }
  })
});


//Update Doctor data
router.put('/update/doctor/:id',verifyToken,function(req,res) 
{
  var id = req.params.id;
   //Get token from the header
  var token   = req.headers.authorization;
  //Decode the token into json object
  var decoded = jwt_decode(token);
  var updated = { "updatedby" : decoded.id }
  var request = {...req.body,...updated}
  Users.update(request,{where:{id:id,is_doctor:'1'}}).then(function(user_update)
  {
    if (user_update == 1) 
    {
       res.send("User_Doctor updated successfully.");
    } 
    else
    {
       res.send("Error in updating Users");
    }
  })
});


//Update Admin data
router.put('/update/admin/data/:id',verifyToken,function(req,res)
{
  var id = req.params.id;
   //Get token from the header
  var token   = req.headers.authorization;
  //Decode the token into json object
  var decoded = jwt_decode(token);
  var updated = { "updatedby" : decoded.id }
  var request = {...req.body,...updated}
  Users.update(request,{where:{id:id,is_doctor:'0'}}).then(function(user_update)
  {
    if (user_update == 1) 
    {
      res.send("User_Admin was updated successfully.");
    } 
    else
    {
      res.send("Error in updating Users");
    }
  })
});



// //User Login 
// router.post('/login/page',function(req,res)
// {
//   Users.findOne({where:{email_id:req.body.email_id,password:req.body.password}}).then(function(user)
//   {
//     if(user)
//     {
//       token = jwt.sign({user}, config.get('secret'));
//       res.status(200).json({ token : token });
//     } 
//     else 
//     {
//       res.status(404).json({ error : "User does not exist" });
//     }
//   })
// });


//User Login
router.post('/login/page',function(req,res)
{
  Users.findOne({where:{email_id:req.body.email_id,password:req.body.password,is_active:'1'}}).then(function(user)
  {
    if(user)
    {
      User_role_mapping.findOne({where:{user_id_fk:user.id,is_active:'1'}}).then(function(map_data)
      {
         Roles.findOne({where:{id:map_data.role_id_fk}}).then(function(role_data)
         {
            token = jwt.sign({id:user.id,email_id:user.email_id,user_name:user.user_name,mobile_number:user.mobile_number,role:role_data.role_name}, config.get('secret'));
            res.status(200).json({ token : token });
         })
      })
    }
    else
    {
      res.status(404).json({ error : "User does not exist" });
    }
  })
});

// doctor count
router.get('/get/doctor/count/numbers',verifyToken,function(req,res){	
	Users. findAndCountAll({where:{ is_active:'1',is_doctor:'1' }}).then(function(users){ 
	
		return res.send({count:users.count})
	})
})

//Get list by Id
router.get('/admin/data/:id',verifyToken,function(req,res)
{
  Users.findAll({where: {id: req.params.id,is_active:'1',is_doctor:'0'}}).then(function(Users)
  {
    return res.send(Users);
  })
});
//Get list by Id
router.get('/doctor/list/data/:id',verifyToken,function(req,res)
{
  Users.findAll({where: {id: req.params.id,is_active:'1',is_doctor:'1'}}).then(function(Users)
  {
    return res.send(Users);
  })
});



//get by id for updatation for need
router.get('/:id',verifyToken,function(req,res)
{
  Users.findOne({where: {id: req.params.id,is_active:'1',is_doctor:'1'}}).then(function(user_data)
  {
    if(user_data.is_doctor == '1')
    {
      var new_data =  user_data.dataValues;
      Doctor_specialist_mapping.findOne({where: {user_id_fk:user_data.id}}).then(function(data)
      {
        var mapping_data_id = data.specialist_id_fk;
        Master_specialists.findOne({where: {id:mapping_data_id}}).then(function(data)
        {
         
            var master_data = 
            {
              "speciallation_name" : data.speciallation_name
            }
          var result = merge(new_data,master_data);
          return res.send(result);
        })
      })
    }
    else
    {
      return res.send(user_data);
    }
  })
});

router.post('/new/data',verifyToken,function (req, res) 
{  
  //Get token from the header
  var token   = req.headers.authorization;
  //Decode the token into json object
  var decoded = jwt_decode(token);
  //merge two json objects
  var created = { "createdby" : decoded.id }
  var user_id_fk = { "user_id_fk" : decoded.id }
  var request = {...created,...user_id_fk,...req.body};
 
  console.log("hii")
	user_personal_details.create(request).then(function(doctor)
  {  
console.log("hello")
    res.status(200).send(" created successfully")
    
  })

});
// update a personal details by using decoded id
// update a personal details by using decoded id
router.put('/update/personal/:user_id_fk',verifyToken,function(req,res)
{
  var user_id_fk = req.params.user_id_fk;
  var token   = req.headers.authorization;
  var decoded = jwt_decode(token);
  var updated = { "updatedby" : decoded.id }
  var request = {...updated,...req.body};
  user_personal_details.update(request,{where:{user_id_fk:decoded.id}}).then(function(user_personal_details){
    {
      res.send("user_personal_details updated successfully");
    }
  })
});


// user_personal_details get
router.get('/personal/list',verifyToken,function(req,res)
{
   //Get token from the header
   var token   = req.headers.authorization;
   //Decode the token into json object
   var decoded = jwt_decode(token);
  user_personal_details.findOne({where:{user_id_fk:decoded.id}}).then(function( user_personal_details)
  {
    return res.send( user_personal_details);
  })
});


//Get hms data testing 
router.get('/data', function (req, res) {
  res.send('WELCOME_HMS_2022');
});


/////update doctor and specialist
router.put('/update/:id',verifyToken,function(req,res)
{
  var id = req.params.id;
   //Get token from the header
  var token  = req.headers.authorization;
  //Decode the token into json object
  var decoded = jwt_decode(token);
  var updated = { "updatedby" : decoded.id }
  var request = {...req.body,...updated}
  Users.update(request,{where:{id:id,is_doctor:'1'}}).then(function(user_update)
  {
        var request_update =
        {
          "specialist_id_fk":req.body.specialist_id_fk,
          "updatedby" : decoded.id 
        }
        Doctor_specialist_mapping.update(request_update,{where:{user_id_fk:id}}).then(function(update)
        {
          console.log(update)
          res.send("User_Doctor updated successfully.");
        })
      
      
  })
});


//Add profile
router.post('/new/image',upload.single("image"),verifyToken,function (req, res) 
{  
    //console.log(req);
    //Get token from the header
    var token   = req.headers.authorization;
    //Decode the token into json object
    var decoded = jwt_decode(token);
    //merge two json objects
    var created = 
    {
      user_id_fk:decoded.id,
      createdby : decoded.id,
      "image":url+req.file.filename } 
    var request = {...created,...req.body};
    //console.log("file",req.file);
  user_image_mapping.create(request).then(function(Products,err)
   {         
          res.status(200).send("image upload success");
        })
,function(err){
    if(err && err.errors[0].message) 
    { 
      return res.status(500).send(err.errors[0].message)
    }
    res.status(500).send("image upload filed");
  }
});

//Get Profile 
router.get('/profile/image/get',verifyToken,function(req,res)
{
   //Get token from the header
   var token   = req.headers.authorization;
   //Decode the token into json object
   var decoded = jwt_decode(token);
  user_image_mapping.findOne({where:{user_id_fk:decoded.id,is_active:'1'}}).then(function(user)
  {
    res.send(user)
  })
});

//update profile
router.put('/profile/update/:user_id_fk',upload.single("image"),verifyToken,function(req,res)
{
  // var id = req.params.id;
  // var user_id_fk = decoded.id
  var token   = req.headers.authorization;
  var decoded = jwt_decode(token);
  var updated =
  {
    updatedby : decoded.id,
   // "image":url+req.file.originalname
    "image":url+req.file.filename
  }
  var request = {...updated,...req.body};
  user_image_mapping.update(request,{where:{user_id_fk:decoded.id}}).then(function(user_image){
    {
      res.send("user_image updated successfully");
    }
  })
});

// search method
router.get('/search',function(req,res){
  var request=req.body;
  var find=req.body.find;
  //console.log(request)
  Users.findAll({where:{is_doctor:1,is_active:1,[Op.or]:{user_name: {[Op.like]:'%'+find+'%'}}
  }}).then(function(Users){
  return res.send(Users)
  })
})

/// working apii........................................................
//Add New Users
router.post('/new/val',verifyToken,function(req,res)
{
  //Get token from the header
  var token   = req.headers.authorization;
  //Decode the token into json object
  var decoded = jwt_decode(token);
  const {user_name, password,email_id,mobile_number,gender,}=req.body;
  var request = req.body;
  let errors =[];
  Users.findOne({where:{email_id:email_id,is_active:1}}).then(user=>{
   // console.log(user)

    if(user){
      errors.push({msg:"already exist"});
      res.send("user email_id already exist")
    }
    else {
      
      Users.findOne({where:{mobile_number:mobile_number,is_active:1}}).then(userid=>{
        if(userid){
          errors.push({msg:"already exist"});
          res.send("user mobile_number already exist")
        }
        else{
          var random_number = 
          {
            "user_random_number" : "USR"+Math.floor(100000 + Math.random() * 900000),
            "createdby" : decoded.id
          }
          //merge two json objects
          var user_data_payload = {...request,...random_number};
          Users.create(user_data_payload).then(function(data)
          {
            if (data.is_doctor == '1')
            {
              var doctor_payload =
              {
                "user_id_fk" : data.id,
                "specialist_id_fk"   : req.body.specialist_id,
                "createdby" : data.createdby,
                "updatedby" : data.updatedby
              }
              Doctor_specialist_mapping.create(doctor_payload).then(function(doctor_data){});
              //Mapping the roles for Doctor
              Roles.findOne({where:{role_name:"Doctor"}}).then(function(role_data)
              {
                var map_data = 
                {
                  "role_id_fk" : role_data.id,
                  "user_id_fk" : data.id,
                  "createdby"  : data.createdby,
                  "updatedby"  : data.updatedby
                }
                User_role_mapping.create(map_data).then(function(role_map)
                {
                  res.send("User_doctor created successfully");
                });
              })  
            }
            else
            {
              //Mapping the role for Admin
              Roles.findOne({where:{role_name:"Admin"}}).then(function(role_data)
              {
                var map_data = 
                {
                  "role_id_fk" : role_data.id,
                  "user_id_fk" : data.id,
                  "createdby"  : data.createdby,
                  "updatedby"  : data.updatedby
                }
                User_role_mapping.create(map_data).then(function(role_map)
                {
                  res.send("User_admin created successfully");
                });
              }) 
            }
          })
        }
      })
 

    }
  })
})

// search doctor

router.post('/role/doctor/search',verifyToken,function(req,res)
{
  var request=req.body;
  var find=req.body.find;
  var response_array = [];
  Users.findAll({where:{[Op.or]:{full_name: {[Op.like]:'%'+find+'%'},mobile_number: {[Op.like]:'%'+find+'%'}} ,is_active:1,is_doctor:'1'},order:[['id','DESC']]}).then(function(user)
  {
    var processItems = function(x)
    {
      if( x < user.length )
      {
        Doctor_specialist_mapping.findOne({where:{user_id_fk:user[x].id},attributes:['specialist_id_fk']}).then(function(response)
        {
          Master_specialists.findOne({where:{id:response.specialist_id_fk},attributes:['speciallation_name']}) .then(function(res)
          {
            var data = 
            {
              "id": user[x].id,
              "fullname" : user[x].full_name,
              "emailid" :user[x].email_id,
              "mobile_number" : user[x].mobile_number,
              "gender": user[x].gender,
              "education": user[x].education,
              "experience":user[x].experience,
              "speciallation_name" :res.speciallation_name
            }
            response_array.push(data)
            processItems(x+1);
          })
        })
      }
      else 
      {
        res.status(200).send({"user_data": response_array})
      }
    }
    processItems(0)
  })
});


//Get Admin list
router.post('/role/admin/search',verifyToken,function(req,res)
{
  var request=req.body;
  var find=req.body.find;
  Users.findAll({where:{[Op.or]:{full_name: {[Op.like]:'%'+find+'%'},mobile_number: {[Op.like]:'%'+find+'%'}} ,is_active:1,is_doctor:'0'},order:[['id','DESC']]}).then(function(Users)
  {
    return res.send(Users);
  })
});

// update or post call
router.put('/upload/:user_id_fk',verifyToken,function(req,res)
{
  var user_id_fk = req.params.user_id_fk;
  var token   = req.headers.authorization;
  var decoded = jwt_decode(token);
  user_personal_details.findOne({where:{user_id_fk:user_id_fk}}).then(user_data=>{
    if(user_data){
      var updated = { "updatedby" : decoded.id }
      var request = {...updated,...req.body};
      Users.update(request,{where:{id:decoded.id}}).then(function(user){
        var payload ={
          "updatedby" : decoded.id ,...req.body }
      user_personal_details.update(payload,{where:{user_id_fk:decoded.id}}).then(function(details){
        res.send ("updated sucess")
      })
      })
    }else{
      Users.findOne({where:{id:user_id_fk,is_active:"1",is_doctor:"1"}}).then(use=>{
        if(use){
          var created = { "createdby" : decoded.id,
                       "user_id_fk":decoded.id }
          var request = {...created,...req.body};
          user_personal_details.create(request).then(function(detail){
            res.send("created sucessfull")
          })

        }else{
          res.send("invalid")
               }
      })
     
    }
  })
})

//get profile for doctor profile
router.get('/get/profile',verifyToken,function(req,res)
{
    
  var token   = req.headers.authorization;
  var decoded = jwt_decode(token);
  Users.findOne({where:{id:decoded.id,is_active:'1'}}).then(function(products)
  {
    user_personal_details.findOne({where:{user_id_fk:products.id}}).then(function( user_personal_details)
    {
    //  data={...user_personal_details}
      user_image_mapping.findOne({where:{user_id_fk:user_personal_details.user_id_fk}}).then(function(user_image)
      {  
        Doctor_specialist_mapping.findOne({where:{user_id_fk:decoded.id},attributes:['specialist_id_fk']}).then(function(specialist)	
        {		
          Master_specialists.findOne({where:{id:specialist.specialist_id_fk}}).then(function(master)
          {
       var data={...products.dataValues,...user_personal_details.dataValues,...user_image.dataValues,...specialist.dataValues,...master.dataValues}
res.send(data)
          })
        })
      })
    })
  })
});

// get profile by conditions
router.get('/get/pro',verifyToken,function(req,res)
{
  var token   = req.headers.authorization;
  var decoded = jwt_decode(token);
  user_personal_details.findOne({where:{user_id_fk:decoded.id,is_active:'1'}}).then(function(products)
{     if(products){
 // console.log("1")
  Users.findOne({where:{id:decoded.id,is_active:'1'}}).then(function(pro)
  {  
    user_personal_details.findOne({where:{user_id_fk:decoded.id,is_active:'1'}}).then(function(users)
    {    
    Doctor_specialist_mapping.findOne({where:{user_id_fk:decoded.id},attributes:['specialist_id_fk']}).then(function(specialist)	
    {		
      Master_specialists.findOne({where:{id:specialist.specialist_id_fk}}).then(function(master)
      {
   var data={...pro.dataValues,...users.dataValues,...specialist.dataValues,...master.dataValues}
res.send(data)
      })
    })
    })

})
}else{
  //console.log("2")
  
  Users.findOne({where:{id:decoded.id,is_active:'1'}}).then(function(pro)
  {     
    Doctor_specialist_mapping.findOne({where:{user_id_fk:decoded.id},attributes:['specialist_id_fk']}).then(function(specialist)	
    {		
      Master_specialists.findOne({where:{id:specialist.specialist_id_fk}}).then(function(master)
      {
   var data={...pro.dataValues,...specialist.dataValues,...master.dataValues}
res.send(data)
      })
    })
  

})

}
})

})

 ///image upload and update
 router.put('/image/upload',upload.single("image"),verifyToken,function(req,res)
 {
   var user_id_fk = req.params.user_id_fk;
   var token   = req.headers.authorization;
   var decoded = jwt_decode(token);
   user_image_mapping.findOne({where:{user_id_fk:decoded.id,is_active:"1"}}).then(user_data=>{
     if(user_data){
       var updated = { "updatedby" : decoded.id ,
       "image":url+req.file.filename
     }
       var request = {...updated,...req.body};
       user_image_mapping.update(request,{where:{user_id_fk:decoded.id}}).then(function(user){
         
         res.send ("updated sucess")
     
       })
     }else{
       var created = 
       {
         user_id_fk:decoded.id,
         createdby : decoded.id,
         "image":url+req.file.filename } 
       var request = {...created,...req.body};
       //console.log("file",req.file);
     user_image_mapping.create(request).then(function(Products,err)
      {         
             res.status(200).send("image upload success");
           })
   ,function(err){
       if(err && err.errors[0].message) 
       { 
         return res.status(500).send(err.errors[0].message)
       }
       res.status(500).send("image upload filed");
     }
                }
       
      
    
   })
 })

 ///super admin betwwen dates
router.post('/patient/between',verifyToken,function(req,res)
{
  //Get token from the header
  var token   = req.headers.authorization;
  //Decode the token into json object
  var decoded = jwt_decode(token);
  //merge two json objects
  var created = { "createdby" : decoded.id }
  // console.log(created)
  var user_id_fk = req.body.user_id_fk;
    var response_array = [];
    var patient_details;
if(user_id_fk){
  console.log("1")
    Patient_doctor_mapping.findAll({where:{user_id_fk:user_id_fk,is_active:1},attributes:['patient_id_fk']}).then(function(patient_map)
    {
      var processItems = function(x)
      {
        if( x < patient_map.length )
        {
         
        Patient_personal_details.findOne({where:{id:patient_map[x].patient_id_fk}}).then(function(response)
          {
            Patient_medical_history.findOne({where:{patient_id_fk:patient_map[x].patient_id_fk}}) .then(function(res)
            {
              Patient_address_details.findOne({where:{patient_id_fk:patient_map[x].patient_id_fk}}) .then(function(address)
              {
               
                  //  console.log(prescription)
                 var data = {...patient_map.dataValues,...response.dataValues,...res.dataValues,...address.dataValues}
                 var current_date = data.createdAt.toLocaleDateString();
                 var date = moment(current_date).format("YYYY/MM/DD");
         var from_date = Date.parse(req.body.start_date);
                         var to_date = Date.parse(req.body.end_date);
                         var check_date = Date.parse(date);
                        if(check_date <= to_date && check_date >= from_date)  
              response_array.push(data)
              processItems(x+1);
                
            })
            })
          })
        }
        else 
        {
          res.status(200).send({"patient_data": response_array})
        }
      }
      processItems(0)
    })
  }else{
    console.log("2")
    Patient_personal_details.findAll({where:{is_active:1}}).then(function(patient_basic)
    {
     	var patient_payload = patient_basic;
        var processItems = function(x)
        {
            if( x < patient_payload.length ) 
            {
            	Patient_medical_history.findOne({where:{patient_id_fk:patient_payload[x].id}}).then(function(patient_data)
            	{
                	var data = {...patient_payload[x].dataValues,...patient_data.dataValues}
            		Patient_address_details.findOne({where:{patient_id_fk:patient_payload[x].id}}).then(function(patient_medical)
            		{
                        var additional_data ={...data,...patient_medical.dataValues}
                        Patient_doctor_mapping.findOne({where:{patient_id_fk:patient_payload[x].id}}).then(function(map_data){
                        	Users.findOne({where:{id:map_data.user_id_fk}}).then(function(user_data)
                        	{
                        		var user = 
                        		{
                        			"user_name" : user_data.dataValues.user_name
                        		}
                        		var final_data = {...additional_data,...user}
	                			var current_date = final_data.createdAt.toLocaleDateString();
	                			var date = moment(current_date).format("MM/DD/YYYY");
								var from_date = Date.parse(req.body.start_date);
                                var to_date = Date.parse(req.body.end_date);
                                var check_date = Date.parse(date);
                               if(check_date <= to_date && check_date >= from_date)  
                           	    {
	                			response_array.push(final_data)
	                		    }
	                			processItems(x+1);
                	    	})
                        })
                        
            		})
        		})
            }
            else 
            {
          		res.status(200).send({"patient_data":response_array})
        	}
    	}
    	processItems(0)
  	})
  }
  })
//request to mail for password validation
  //verify otp 
router.post('/verify/otp', function (req, res) {
  User_validation.findOne({ where: { email_id: req.body.email_id, otp: req.body.otp } }).then(function (user) {
    if (user) {
      User_validation.destroy({ where: { email_id: req.body.email_id } }).then(function (user_validate) {
        res.status(200).send("success");
      })
    }
    else {
      res.send("Try again");
    }
  })
})

// otp send  
router.post('/send/otp/password', function (req, res) {
  Users.findOne({ where: { email_id: req.body.email_id } }).then(function (users) {

    if (users) {
      var payload =
      {
        "random_number": "Your otp is " + Math.floor(1000 + Math.random() * 9000),
        "subject": "Password Validation"
      }
      var data = { ...payload, ...req.body }
      var email = sendMail(data);
      var number = data.random_number.match(/[0-9]+/g);
      var user_payload =
      {
        "email_id": data.email_id,
        "otp": number[0]
      }
      User_validation.create(user_payload).then(function (user_validate) {
        res.send("success")
      })
    } else {
      res.send("user invalid")
    }
  })
})

//Password updated
router.put('/update/password/reset', function (req, res) {
  var data =
  {
    "password": req.body.password
  }
  Users.update(data, ({ where: { email_id: req.body.email_id } })).then(function (users) {
    res.send("Password updated")
  })
})

module.exports = router;
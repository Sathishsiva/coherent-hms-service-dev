var express       = require('express'),
    router        = express.Router(),
    bodyParser    = require('body-parser');
var verifyToken    = require('../verifyToken.js');
const jwt_decode = require('jwt-decode');
const  Sequelize = require('sequelize');
const Op = Sequelize.Op
var Master_specialists = __db.Master_specialists;

router.use(bodyParser.json());

//Add Specialization
router.post('/new',verifyToken,function (req, res) 
{  
  //Get token from the header
  var token   = req.headers.authorization;
  //Decode the token into json object
  var decoded = jwt_decode(token);
  //merge two json objects
  var created = {
  "createdby" : decoded.id ,
  "speciallation_code" : "SPC"+Math.floor(10000 + Math.random() * 90000)
}
  var request = {...created,...req.body};
	Master_specialists.create(request).then(function(Master_specialists)
  {
    res.status(200).send("Master_specialists created successfully")
	})
});

//Get Specialization List
router.get('/',verifyToken,function(req,res)
{
  Master_specialists.findAll({where:{is_active:'1'},order:[['id','DESC']]}).then(function(Master_specialists)
  {
    return res.send(Master_specialists);
  })
});

//Get Specialization list by Id
router.get('/:id',verifyToken,function(req,res)
{
  Master_specialists.findAll({where: {id: req.params.id}}).then(function(Master_specialists)
  {
    return res.send(Master_specialists);
  })
});

//Deactivate the Specialization
router.put('/deactivate/:id',verifyToken,function(req, res) 
{  
	var id = req.params.id;
  //Get token from the header
  var token   = req.headers.authorization;
  //Decode the token into json object
  var decoded = jwt_decode(token);
  Master_specialists.update({is_active:'0',updatedby:decoded.id},{where:{id:id}}).then(function(Master_specialists) 
  {
    if(Master_specialists > 0) 
    {
      res.send("The masterId: "+id+" is deactivated successfully")
    }
	})
});

//Update the Specialization data
router.put('/update/data/:id',verifyToken,function(req,res) 
{
  var id = req.params.id;
  //Get token from the header
  var token   = req.headers.authorization;
  //Decode the token into json object
  var decoded = jwt_decode(token);
  var updated = { "updatedby" : decoded.id }
  var request = {...updated,...req.body};
  Master_specialists.update(request,{where:{id:id}}).then(function(Master_specialists)
  {
    if (Master_specialists == 1) 
    {
      res.send("Master_specialists updated successfully");
    } 
  })
});



//Get Specialization search
router.post('/search/get',verifyToken,function(req,res)
{
  var request=req.body;
  var find=req.body.find;
  Master_specialists.findAll({where:{[Op.or]:{speciallation_type: {[Op.like]:'%'+find+'%'},speciallation_name: {[Op.like]:'%'+find+'%'}},is_active:1},order:[['id','DESC']]}).then(function(Master_specialists)
  {
    return res.send(Master_specialists);
  })
});
// validation for specialist
router.post('/new/valid',verifyToken,function(req,res)
{
  //Get token from the header
  var token   = req.headers.authorization;
  //Decode the token into json object
  var decoded = jwt_decode(token);
  const {speciallation_name,speciallation_code,speciallation_type}=req.body;
  var request = req.body;
  let errors =[];
  Master_specialists.findOne({where:{speciallation_name:speciallation_name}}).then(master=>{
    if(master){
      errors.push({msg:"already exist"});
      res.send(" speciallation_name already exist")
    }
   else{
    Master_specialists.findOne({where:{speciallation_type:speciallation_type}}).then(specialist=>{
      if(specialist){
        errors.push({msg:"already exist"});
        res.send("speciallation_type already exist")
      }
      else{
        var random_number =
        {
          speciallation_code : "SPC"+Math.floor(10000 + Math.random() * 90000),
          createdby : decoded.id
        }
        var master_data_payload = {...request,...random_number};
        Master_specialists.create(master_data_payload).then(function(data)
        {
          res.send("master_specialist created successfully");
        })
      }
    })
    }
  })
})
module.exports = router;